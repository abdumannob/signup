import React from "react";
import Foundimage from "./img/found.png";

function Json() {
  return (
    <div>
      <div className="myfon flex justify-center items-center">
        <div>
          <img src={Foundimage} alt="404 found" />
          <div className="flex justify-center mt-5">
            <button
              type="button"
              className="bg-buttons text-white w-60 mt-4 py-2 rounded-md me-1"
            >
              Go Home Page
            </button>
            <button
              type="button"
              className=" text-textcolor w-60 mt-4 py-2 border-2 border-color border-solid rounded-md ms-2"
            >
              Reload Page
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Json;
