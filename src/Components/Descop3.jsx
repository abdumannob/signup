import React from "react";
import { CloudIcon, SearchIcon, SoundIcon } from "./Icon";
import GirlImg from "./img/girlimg.png";

export default function Descop1() {
  return (
    <div className="myfon ">
      <div className="w-11/12 mx-auto h-auto">
        <div className=" h-20 flex justify-between">
          <div className="flex flex-wrap md:justify-between items-center lg:w-1/3">
            <CloudIcon />
            <h1 className="text-lg tracking-wider font-bold text-textwhite me-3 lg:me-0">
              <span className="text-textcolor font-bold">Books</span> List
            </h1>
            <div className="flex">
              <SearchIcon />
              <p className="opacity-50 text-textwhite">
                Search for any training you want
              </p>
            </div>
          </div>
          <div className="flex items-center ">
            <SoundIcon />
            <div className="rounded-[50%] gradiend  w-10 h-10 flex justify-center items-center bg-slate-100 ms-6">
              <img
                className="rounded-[50%] w-8 h-8 "
                src={GirlImg}
                alt="Rasm topilmadi"
              />
            </div>
          </div>
        </div>
        {/* // */}
        <div>
          <div className="flex justify-between mt-12  flex-wrap ">
            <h1 className="text-textwhite text-4xl font-bold">
              You’ve got <span className="text-textcolor">7 book </span>
            </h1>
            <div className="flex flex-wrap mt-5 xl:mt-0">
              <input
                className="w-96 py-3.5 ps-4 rounded-md border-spacing-1 border-slate-300 border-2 "
                type="text"
                placeholder="Enter your name"
              />
              <button
                type="button"
                className="bg-buttons text-white w-60 mt-4 md:mt  py-3.5 rounded-md md:ms-6 border-color border-2"
              >
                + Create a book
              </button>
            </div>
          </div>
          {/*Create book*/}
          {/* <div className="w-[430px] h-[573px] flex items-center bg-bgfon rounded-xl create firstdiv ">
            <div className="w-4/5 h-[90%] mx-auto">
              <div>
                <div className="flex justify-between">
                  <h1 className="text-xl font-semibold">Create a book</h1>
                  <button type="button">
                    <Delete2Icon />
                  </button>
                </div>
                <form className="flex flex-col gap-y-4 mt-6">
                  <div>
                    <label
                      for="title"
                      className="text-sm font-semibold block leading-4"
                    >
                      Title
                    </label>
                    <input
                      type="text"
                      id="title"
                      placeholder="Enter your title"
                      className="py-3 ps-2 w-full border-2 rounded-md mt-1"
                    />
                  </div>
                  <div>
                    <label
                      for="author"
                      className="text-sm font-semibold block leading-4"
                    >
                      Author
                    </label>
                    <input
                      type="text"
                      id="author"
                      placeholder="Enter your author"
                      className="py-3 ps-2 w-full border-2 rounded-md mt-1"
                    />
                  </div>
                  <div>
                    <label
                      for="cover"
                      className="text-sm font-semibold block leading-4"
                    >
                      Cover
                    </label>

                    <div className="flex border-2 rounded-md items-center  mt-1">
                      <div className="ms-2 opacity-50">
                        <CovertIcon />
                      </div>
                      <input
                        type="text"
                        id="cover"
                        placeholder="Enter your cover"
                        className="py-3 ps-2 w-full "
                      />
                    </div>
                  </div>
                  <div>
                    <label
                      for="published"
                      className="text-sm font-semibold block leading-4"
                    >
                      Published
                    </label>

                    <div className="flex border-2 rounded-md items-center  mt-1">
                      <div className="ms-2">
                        <DataIcon />
                      </div>
                      <input
                        type="text"
                        id="published"
                        placeholder="Enter your published"
                        className="py-3 ps-2 w-full "
                      />
                    </div>
                  </div>
                  <div>
                    <label
                      for="pages"
                      className="text-sm font-semibold block leading-4"
                    >
                      Pages
                    </label>
                    <input
                      type="text"
                      id="pages"
                      placeholder="Enter your pages"
                      className="py-3 ps-2 w-full border-2 rounded-md  mt-1"
                    />
                  </div>
                  <div className="flex gap-3">
                    <button
                      type="button"
                      className=" text-textcolor w-1/2 py-2 border-2 text-base font-semibold border-color border-solid rounded-md"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      className="bg-buttons text-white w-1/2  py-2 rounded-md border-color border-2"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div> */}

          <h2 className="text-xl text-textwhite mt-3">Your task today</h2>
        </div>
        <div className="flex-wrap flex gap-8">
          <div className="w-[400px] h-auto bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-4/5">
              <h1 className="font-semibold">Raspberry Pi User Guide</h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Nulla adipiscing neque
                varius vestibulum magna in. Tortor quisque nisl congue ut tellus
                sem id.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  211 pages
                </p>
              </div>
            </div>
          </div>
          {/* // */}
          <div className="w-[394px] bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-[85%]">
              <h1 className="font-semibold w-3/5">
                Lorem ipsum dolor sit amet consectetur.
              </h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Consequat nunc eu a
                lacus rhoncus mollis.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  1248 pages
                </p>
              </div>
            </div>
          </div>
          {/* / */}
          <div className="w-[394px] bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-4/5">
              <h1 className="font-semibold">Lorem ipsum</h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Etiam auctor vitae morbi
                cursus id non. Cursus id semper ipsum nunc. Adipiscing massa ut
                morbi mattis proin. Mi id sit vulputate bibendum.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  587 pages
                </p>
              </div>
            </div>
          </div>
          {/* / */}
          <div className="w-[394px] bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-4/5">
              <h1 className="font-semibold">Lorem ipsum dolor sit</h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Id suspendisse nascetur
                elit laoreet ornare augue interdum. Sociis mattis senectus
                vulputate nisi leo urna. Accumsan ornare consectetur semper
                convallis ultricies quam.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  119 pages
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
