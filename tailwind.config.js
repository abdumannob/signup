/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        buttons: "#6200EE",
        bgfon: "#FEFEFE",
        bgbin: "#EFE6FD",
      },
      textColor: {
        textcolor: "#6200EE",
        textwhite: "#FEFEFE",
        textbin: "#9654F4",
      },
      borderColor: {
        color: "#6200EE",
      },
    },
  },
  plugins: [],
};
