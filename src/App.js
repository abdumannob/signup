import React from "react";
import Signup from "./Components/signup";
// import Signin from "./Components/Signin";
// import Descop3 from "./Components/Descop3";
// import Descop4 from "./Components/Descop4";
// import Nofound from "./Components/Nofound";

function App() {
  return (
    <div>
      <div className="container mx-auto">
        <Signup />
        {/* <Signin />
        <Descop3 />
        <Descop4 />
        <Nofound /> */}
      </div>
    </div>
  );
}

export default App;
