import React from "react";
import { CloudIcon, Search2Icon, DeleteIcon, SoundIcon } from "./Icon";
import GirlImg from "./img/girlimg.png";

export default function Descop4() {
  return (
    <div className="myfon ">
      <div className="w-11/12 mx-auto">
        <div className=" h-20 flex justify-between">
          <div className="flex flex-wrap gap-x-3 items-center lg:w-1/3">
            <CloudIcon />
            <h1 className="text-lg tracking-wider font-bold text-textwhite me-3 lg:me-0">
              <span className="text-textcolor font-bold">Books</span> List
            </h1>
            <div className="flex bg-white w-11/12 max-w-[300px] px-4 py-2 rounded justify-between">
              <div className="flex">
                <Search2Icon />
                <p className="  ms-2">Raspberry</p>
              </div>
              <DeleteIcon />
            </div>
          </div>
          <div className="flex items-center ">
            <SoundIcon />
            <div className="rounded-[50%] gradiend  w-10 h-10 flex justify-center items-center bg-slate-100 ms-6">
              <img
                className="rounded-[50%] w-8 h-8 "
                src={GirlImg}
                alt="Rasm topilmadi"
              />
            </div>
          </div>
        </div>
        {/* // */}
        <div>
          <div className="flex justify-between mt-12  flex-wrap ">
            <h1 className="text-textwhite text-4xl font-bold">
              You’ve got <span className="text-textcolor">7 book </span>
            </h1>
            <div className="flex flex-wrap mt-5 xl:mt-0">
              <input
                className="w-96 py-3.5 ps-4 border-2  rounded "
                type="text"
                placeholder="Enter your name"
              />
              <button
                type="button"
                className="bg-buttons text-white w-60  py-3.5 rounded-md md:ms-6 border-color border-2"
              >
                + Create a book
              </button>
            </div>
          </div>
          <h2 className="text-xl text-textwhite mt-3">Your task today</h2>
        </div>
        <div className="flex-wrap flex gap-8 mt-5">
          <div className="w-[400px] h-auto bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-4/5">
              <h1 className="font-semibold">Raspberry Pi User Guide</h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Nulla adipiscing neque
                varius vestibulum magna in. Tortor quisque nisl congue ut tellus
                sem id.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  211 pages
                </p>
              </div>
            </div>
          </div>
          {/* // */}
          <div className="w-[394px] bg-bgfon rounded-2xl flex justify-center items-center py-8 shadoww">
            <div className="w-[85%]">
              <h1 className="font-semibold w-3/5">
                Raspberry ipsum dolor sit amet consectetur.
              </h1>
              <p className="my-3">
                Lorem ipsum dolor sit amet consectetur. Consequat nunc eu a
                lacus rhoncus mollis.
              </p>
              <div className="flex justify-between">
                <p className="">Eben Upton: 2012-year</p>
                <p className="bg-bgbin inline text-textbin px-3 rounded-xl ">
                  1248 pages
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
