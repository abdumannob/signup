import React from "react";
import { GoogleIcon, FacebookIcon } from "./Icon";

function Signup() {
  return (
    <div>
      <div className="myfon flex justify-center items-center">
        <div className="w-[430px] h-[695px] firstdiv rounded-xl">
          <div>
            <h1 className="text-4xl font-bold text-center my-11">Sign up</h1>
            <div className="w-11/12 border-solid h-11 border-black border-2 rounded flex justify-center items-center mx-auto my-4">
              <div className="flex ">
                <GoogleIcon />
                <p className="text-l ps-4">Continue with Google</p>
              </div>
            </div>
            <div className="w-11/12 border-solid h-11 border-black border-2 rounded flex justify-center items-center mx-auto my-4">
              <div className="flex">
                <FacebookIcon />
                <p className="text-l ps-4">Continue with Facebook</p>
              </div>
            </div>
            <div className="flex justify-between items-center w-11/12 mx-auto my-8">
              <div className="w-full h-[1px] bg-slate-800"></div>
              <p className="mx-3">OR</p>
              <div className="w-full h-[1px]  bg-slate-800"></div>
            </div>
            <form>
              <div className="mx-auto w-11/12">
                <label for="name">Your name</label>
                <input
                  id="name"
                  type="text"
                  placeholder="Enter name"
                  className="w-full py-2 ps-2 border-2 border-slate-300 border-solid rounded-md"
                />
              </div>
              <div className="mx-auto w-11/12 mt-4">
                <label for="email">Your email</label>
                <input
                  id="email"
                  type="text"
                  placeholder="Enter email"
                  className="w-full py-2 ps-2 border-2 border-slate-300 border-solid rounded-md "
                />
              </div>
              <div className="mx-auto w-11/12 mt-4">
                <label for="username">Your username</label>
                <input
                  id="username"
                  type="text"
                  placeholder="Enter username"
                  className="w-full py-2 ps-2 border-2 border-slate-300 border-solid rounded-md "
                />
              </div>
            </form>
            <div className="mx-auto w-11/12 ">
              <button className="bg-buttons text-white w-full mt-4 py-2 border-2 border-solid rounded-md ">
                Submit
              </button>
              <p className="text-sm text-center">
                Already signed up?
                <a href="https://w3schools.com" className="ps-1 text-textcolor">
                  Go to sign in.
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Signup;
